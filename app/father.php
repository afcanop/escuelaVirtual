<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class father extends Model
{
    protected  $fillable =[
        'nombre',
        'tipo_documento',
        'numero_documeto',
        'telefono',
        'direccion',
        'celular',
        'school_id' 
    ];

    public function levels()
    {
        return $this->belongsToMany('App\level');
    }

    public function Schools(){
        return $this->belongsTo('App\school', 'school_id');
    }
}
