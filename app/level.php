<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class level extends Model
{
    protected  $fillable =[
        'nivel',
        'dia',
        'fechaInicio',
        'fechaFinal',
        'periodo',
        'estado'
    ];

    public function fathers()
    {
        return $this->belongsToMany('App\father');
    }
}
