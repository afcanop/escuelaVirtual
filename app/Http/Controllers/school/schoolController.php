<?php

namespace App\Http\Controllers\school;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\school;

class schoolController extends Controller
{
    public function index()
    {
        return view('school.index');
    }
    
    
    public function create()
    {
        return view('school.create');
    }
    
    public function listFull()
    {
        $allSchool = self::GetAllSchool();
        return view('school.schoolList', compact('allSchool'));
    }

    public function SpecificSchool($id)
    {
      $SpecificSchool = school::find($id);
      return view('school.schoolSpecific', compact('SpecificSchool'));
    }

    public function store(Request $request){
         school::create( $request->all() );
        return Redirect()->route('todas_las_escuelas');
    }

    public function edit($id)
    {
        $information = self::GetSchool($id);
        return view('school.edit', compact('information'));
    }

    public function update(request $request, $id)
    {
        $information  = self::GetSchool($id);
        $information->fill($request->all())->save();
        return Redirect()->route('todas_las_escuelas');

    }

    public function delete($id){
        $information = self::GetSchool($id);
        $information->delete();
        return Redirect()->route('todas_las_escuelas');
    }

    protected function GetSchool($id)
    {
        return school::findOrFail($id);
    }

    protected function GetAllSchool()
    {
        return school::orderby('id', 'DESC')->paginate(4);
    }

}
