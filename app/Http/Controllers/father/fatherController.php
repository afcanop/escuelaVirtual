<?php

namespace App\Http\Controllers\father;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\father;
use App\school as colegio; 
use App\level;

class fatherController extends Controller
{
    public function index(){
        return view('father.index');
    }

    public function listFull()
    {
        $GetFatherAll = self::GetFatherAll();
        return view('father.FatherList', compact('GetFatherAll'));
    }

    public function fatherInformation($id){
        $information = father::find($id);
        return view( 'father.Information', compact('information') );
    }

    public function create()
    {
        $colegios= colegio::all()->pluck('nombre','id');
        $level = level::all()->where('estado', 1)->pluck('dia','id');
        return view('father.create', compact('colegios', 'level'));
    }

    public function store(Request $request)
    {
       $father = father::create($request->all());
       $father->levels()->sync($request->get('nivel'));

       return redirect()->route('todos_los_padres')
       ->with('info', 'Etiqueta creada');
    }

    public function Edit($id)
    {
        $information  = self::GetFather($id);
        $colegios= colegio::all()->pluck('nombre','id');
        return view('father.edit', compact('information', 'colegios'));
    }

    public function update(Request $request, $id)
    {
        $information  = self::GetFather($id);
        $information->fill($request->all())->save() ;

        return view('father.index');

    }


    public function delete($id){
        $information = self::GetFather($id);
        $information->delete();
        return view('father.index');
    }

    protected function GetFatherAll()
    {
        return father::orderby('id', 'DESC')->paginate(4);
    }

    protected function GetFather($id)
    {
        return father::findOrFail($id);
    }
}










// }
