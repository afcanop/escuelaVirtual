<?php

namespace App\Http\Controllers\level;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\level;

class levelController extends Controller
{
    public function index()
    {
        return view('level.index');
    }

    public function listFull()
    {
        $allLevel = self::GetAllLevel();
        return view('level.LevelList', compact('allLevel'));
    }

    public function levelInformation($id)
    {
        $levelInformation = self::GetLevel($id);
        return view('level.levelSpecific', compact('levelInformation'));
    }

    public function create()
    {
        return view('level.create');
    }

    public function store(Request $request)
    {
        level::create( $request->all() );
        return redirect()->route('todos_los_niveles');
    }

    public function edit($id)
    {
        $levelInformation = self::GetLevel($id);
        return view('level.edit', compact('levelInformation'));
    }

    public function update(Request $request, $id)
    {
        $information = self::GetLevel($id);
        $information->fill($request->all())->save() ;
        return redirect()->route('todos_los_niveles');
    }

    protected function GetLevel($id)
    {
        return level::findOrFail($id);
    }

    protected function GetAllLevel()
    {
        return level::orderby('id', 'DESC')->paginate(4);
    }
}
