<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class school extends Model
{
    protected $fillable=[
        'nombre',
        'direccion',
        'telefono',
        'estado'
    ];


    public function Fathers()
    {
        return $this->hasMany('App\father');
    }
}
