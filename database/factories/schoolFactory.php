<?php

use Faker\Generator as Faker;

$factory->define(App\school::class, function (Faker $faker) {
    return [
        'nombre' => $faker->sentence(4),
        'dirreccion' => $faker->sentence(10),
        'telefono'=> $faker->unique()->randomNumber,
        'estado' => false,
    ];
});
