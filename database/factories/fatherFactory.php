<?php

use Faker\Generator as Faker;


$factory->define(App\father::class, function (Faker $faker) {
    return [
        'nombre' => $faker->sentence(4),
        'tipo_documento' => $faker->randomElement(['cedula', 'tarjeta_Identidad']),
        'numero_documeto'=>  $faker->unique()->randomNumber,
        'telefono'=> $faker->unique()->randomNumber,
        'celular'=> $faker->unique()->randomNumber,
        'direccion'=> $faker->sentence(10),
        'school_id'=> App\school::all()->random()->id,
    ];
});
