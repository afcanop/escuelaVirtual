<?php

use Faker\Generator as Faker;

$factory->define(App\level::class, function (Faker $faker) {
    return [
        'nivel'=> rand(1,4),
        'dia' => $faker->randomElement(["Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"]),
        'fechaInicio'=>$faker->date($format = 'Y-m-d', $max = 'now'),
        'fechaFinal'=>$faker->date($format = 'Y-m-d', $max = 'now'),
        'periodo' => $faker->randomElement([1, 2]),
        'estado' => $faker->randomElement([True, false]),
    ];
});
