<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);
        $this->call(LevelsTableSeeder::class);
        $this->call(SchoolsTableSeeder::class);
        $this->call(fathersTableSeeder::class);
        //$this->call(UsersTableSeeder::class);
    }
}
