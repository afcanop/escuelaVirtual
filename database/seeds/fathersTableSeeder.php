<?php

use Illuminate\Database\Seeder;

class fathersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\father::class, 20)
            ->create()
            ->each(function (App\father $father) {
                $father->levels()->attach(
                    [
                        rand(1, 4),
                        rand(5, 15)
                    ]
                );

            });
    }
}
// ->each(function () {
// $father->levels()->attach([
//     rand(1, 4),
//     rand(5, 15),
//     rand(1, 20)

// ]);
