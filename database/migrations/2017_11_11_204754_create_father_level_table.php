<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFatherLevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('father_level', function (Blueprint $table) {
            $table->integer('father_id')->unsigned();
            $table->integer('level_id')->unsigned();
            $table->timestamps();

            $table->foreign('father_id')
            ->references('id')->on('fathers')
            ->onDelete('cascade');
            $table->foreign('level_id')
            ->references('id')->on('levels')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('father_level');
    }
}
