<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('levels', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('nivel');
            $table->enum('dia' , ["Lunes","Martes","Miercoles","Jueves","Viernes","Sabado_8","Sabado_10","Sabado_1","Sabado_3"]);
            $table->date('fechaInicio');
            $table->date('fechaFinal');
            $table->enum('periodo' , [1,2]);
            $table->boolean('estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('levels');
    }
}
