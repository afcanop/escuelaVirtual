@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h1>CREAR NUEVA ESCUELA</h1>
            </div>
            <div class="panel-body">
                {!! Form::model($information, 
                    ['route'=>['EditarEscuela.update',  $information->id],  'method'=>'PUT']) !!}
                    @include('school.form')
                {{ csrf_field() }}

                {!! Form::close() !!}
                
            </div>
        </div>
    </div>
@endsection