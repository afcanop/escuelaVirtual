@extends('layouts.app') @section('content')
    <div class="container">
        <div class="container">
            <h1>LISTA DE ESCUELAS</h1>
            <table class="table table-hover">
                    <thead>
                    <th class="text-center ">NOMBRE</th>
                    <th class="text-center ">TELEFONO</th>
                    <th class="text-center ">DIRECCIÓN</th>
                    <th class="text-center ">ESTADO</th>

                    <th class="text-center " colspan='3'><span class="glyphicon glyphicon-cog"></span> OPCIONES</th>
                    </thead>
                    <tbody>
                    @foreach($allSchool as $level)
                        <tr>
                            <td class="text-center " > {{$level->nombre}} </td>
                            <td class="text-center " > {{$level->telefono}} </td>
                            <td class="text-center " > {{$level->direccion}} </td>
                            <td class="text-center">@if($level->estado)<p>EN CURSO </p>@else<p>FINALIZADO</p>@endif                            </td>
                            <td>
                                <a href="{{route('escuela', $level->id)}}" class="btn btn-info"> INFORMACIÓN</a>
                            </td>
                            <td>
                                    <a href="{{route('EditarEscuela', $level->id)}}" class="btn btn-primary">EDITAR</a>
                            </td>
                            <td>
                            {!! Form::open(
                                ['route'=> ['EliminarEscuela', $level->id], 'method'=>'DELETE' ]
                            ) !!}
                            <button type="button" class="btn btn-danger">ELIMINAR</button>
                            {!! Form::close() !!}

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
            </table>
            {{$allSchool->render()}}
        </div>
    </div>
@endsection