<div class="row form-group">
    <div class="col-sm-12 col-md-6">
        {!!  Form::label('nombre', 'NOMBRE DE LA ESCUELA') !!}
        {!!  Form::text('nombre', null,['class' => 'form-control'] ) !!}
    </div>
    <div class="col-sm-12 col-md-6">
        {!!  Form::label('direccion', 'DIRRECCIÓN') !!}
        {!!  Form::text('direccion', null,['class' => 'form-control'] ) !!}
     </div>
</div>
<div class="row form-group">
    <div class="col-sm-12 col-md-6">
        {!!  Form::label('telefono', 'TÉLEFONO') !!}
        {!!  Form::number('telefono', null,['class' => 'form-control'] ) !!}
    </div>
    <div class="col-sm-12 col-md-6">
            <label>ESTADO</label>
            <div class="checkbox">
                <label>
                    {!! Form::checkbox('estado') !!} 
                    @isset($information->estado)
                        {{$information->estado ?'activo':'inactivo'}}
                    @else
                        estado
                    @endisset
                </label>
            </div>
    </div>
</div>
<div class="row form-group">
    <div class="col-md-12">
        <button type="submit" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-floppy-saved"></span> GUARDAR</button>
    </div>
</div>