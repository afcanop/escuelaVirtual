@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h1>INFORMACIÓN DE {{$SpecificSchool->nombre}}</h1>
            </div>
            <div class="panel-body">
                    <span class="label label-info">TIPO DOCUMENTO</span>
                    <hr>
                    <span class="label label-info">TELEFONO</span>
                    <P>{{$SpecificSchool->telefono}}</P>
                    <hr>
                    <span class="label label-info">DIRRECCIÓN</span>
                    <P>{{$SpecificSchool->dirreccion}}</P>
                    <hr>
                    <span class="label label-info">ESTADO</span>
                    @if($SpecificSchool->estado)<p>Activa </p>@else<p>Inactiva</p>@endif
                    <hr>
                    <div class="btn-group" role="group" aria-label="...">
                        <a href="{{route('padres')}}" class="btn btn-default">REGRESAR A LA LISTA</a>
                        <button type="button" class="btn btn-primary">EDITAR INFORMACIÓN</button>
                        <button type="button" class="btn bg-danger">ELIMINAR</button>
                    </div>
                    <hr>
                    <h3>Padres asociados a esta escuela</h3>
                    @foreach ($SpecificSchool->Fathers as $item)
                    <ul>
                        <li>{{$item->nombre}}</li>
                    </ul> 
                    @endforeach
                </div>
        </div>

    </div>
@endsection
{{ csrf_field() }}