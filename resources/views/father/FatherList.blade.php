@extends('layouts.app') @section('content')
	<div class="container">
		<div class="container">
			<h1>LISTA DE PADRES</h1>
			<table class="table table-hover">
				<thead>
				<th class="text-center ">NOMBRE</th>
				<th class="text-center ">NUMERO DOCUMENTO</th>
				<th class="text-center ">TELEFONO</th>
				<th class="text-center">CELULAR</th>
				<th class="text-center " colspan='3'><span class="glyphicon glyphicon-cog"></span> OPCIONES</th>
			</thead>
				<tbody>
				@foreach($GetFatherAll as $father)
					<tr>
						<td class="text-center"> {{$father->nombre}} </td>
						<td class="text-center"> {{$father->numero_documeto}} </td>
						<td class="text-center"> {{$father->telefono}} </td>
						<td class="text-center"> {{$father->celular}} </td>
						<td>
							<a href="{{ route('Informacion_Del_Padre', $father->id)  }}" class="btn btn-info"> INFORMACIÓN</a>
						</td>
						<td>
							<a href="{{ route('EditarPadre', $father->id)  }}" class="btn btn-primary">Editar</a>
						</td>
						<td>
							{!! Form::open(['route'=> ['EliminarPadre', $father->id], 'method'=>'DELETE' ]) !!}
							<button class="btn btn-danger">
								Eliminar
							</button>
							{!! Form::close() !!}
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
			{{$GetFatherAll->render()}}

		</div>
	</div>
@endsection
{{ csrf_field() }}