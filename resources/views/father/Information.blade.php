@extends('layouts.app')
@section('content')

<div class="container">
    <div class="panel panel-primary">
		<div class="panel-heading">
			<h1 class="text-center">INFORMACIÓN DE {{$information->nombre}}</h1>
		</div>
		<div class="panel-body">
			<span class="label label-info">TIPO DOCUMENTO</span>
			<p> {{$information->tipo_documento}}</p>
			<hr>
			<span class="label label-info">TIPO DOCUMENTO</span>
			<p> {{$information->numero_documeto}}</p>
			<hr>
			<span class="label label-info">TELEFONO</span>
			<p> {{$information->telefono}}</p>
			<hr>
			<span class="label label-info">CELULAR</span>
			<p> {{$information->celular}}</p>
			<hr>
			<span class="label label-info">DIRECCIÓN</span>
			<p> {{$information->direccion}}</p>
			<hr>
			<span class="label label-info">NIVELES QUE A CURSADO EL PADRE</span>
			<p>
				@foreach ($information->levels as $level)
					<ul>
						<a href="{{route('Informacion_Del_nivel', $level->id)}}"><li> {{$level->nivel}} </li></a>	
					</ul>
				@endforeach
			</p>
			<hr>
			<span class="label label-info">ESCUELA</span>
			<ul>
				<li>
					<a href="{{route('escuela', $information->school_id)}}">{{$information->Schools->nombre}}</a>
				</li>
			</ul>
			</p>
			<hr>
			<div class="btn-group" role="group" aria-label="...">
				<a href="{{route('padres')}}" class="btn btn-default">REGRESAR A LA LISTA</a>
				<button type="button" class="btn btn-primary">EDITAR INFORMACIÓN</button>
				<button type="button" class="btn bg-danger">ELIMINAR</button>
			</div>
		</div>
	</div>
</div>
@endsection {{ csrf_field() }}
