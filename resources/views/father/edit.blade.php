@extends('layouts.app')
@section('content')
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1>EDITAR INFORMACION DEL PADRE</h1>
        </div>
        <div class="panel-body">
             {!! Form::model($information,  ['route'=>['EditarPadre.update', $information->id],  'method'=>'PUT' ] ) !!}
                @include('father.Form')
                {{ csrf_field() }}
            {!! Form::close() !!} 

            
        </div>
    </div>
    
@endsection