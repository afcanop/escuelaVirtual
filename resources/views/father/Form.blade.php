<div class="row form-group">
    <div class="col-sm-12 col-md-6">
        {!!  Form::label('nombre', 'NOMBRE COMPLETO DEL PADRE') !!}
        {!!  Form::text('nombre', null,['class' => 'form-control'] ) !!}
    </div>
    <div class="col-sm-12 col-md-6">
        {!!  Form::label('tipo_documento', 'TIPO DOCUMENTO') !!}
        {!! Form::select('tipo_documento', 
                ['cedula' => 'cedula', 'tarjeta_Identidad' => 'tarjeta_Identidad'],
                null, ['class' => 'form-control']
        ) !!}
    </div>
</div> 
<div class="row form-group">
    <div class="col-sm-12 col-md-6">
        {!!  Form::label('numero_documeto', 'NÚMERO DEL DOCUMENTO') !!}
        {!!  Form::number('numero_documeto', null,['class' => 'form-control'] ) !!}
    </div>
    <div class="col-sm-12 col-md-6">
        {!!  Form::label('telefono', 'TELEFONO FIJO') !!}
        {!!  Form::number('telefono', null,['class' => 'form-control'] ) !!}
    </div>
</div>
<div class="row form-group">
    <div class="col-sm-12 col-md-6">
        {!!  Form::label('direccion', 'DIRECCCION') !!}
        {!!  Form::text('direccion', null,['class' => 'form-control'] ) !!}
    </div>
    <div class="col-sm-12 col-md-6">
            {!!  Form::label('celular', 'CELULAR') !!}
            {!!  Form::number('celular', null,['class' => 'form-control'] ) !!}
    </div>
</div>
<div class="row form-group">
    <div class="col-sm-6 col-md-6">
        {!!  Form::label('school_id', 'Colegio de inscripcion') !!}
        {!! Form::select('school_id', $colegios,null
            ,['class' => 'form-control'] )  !!}
    </div>
    <div class="col-sm-6 col-md-6">
        {!!  Form::label('nivel', 'niveles') !!}
        {!! Form::select('nivel', $level,null
            ,['class' => 'form-control'] )  !!}
    </div>
</div>
<div class="row form-group">
    <div class="col-md-12">
        <button type="submit" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-floppy-saved"></span> GUARDAR</button>
    </div>
</div>
