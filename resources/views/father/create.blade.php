@extends('layouts.app')
@section('content')
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1>REGISTRAR UN NUEVO PADRE</h1>
        </div>
        <div class="panel-body">
            {!! Form::open(
                ['route'=>['crearPadre.store'],  'method'=>'POST' ]
            ) !!}
                @include('father.Form')
                {{ csrf_field() }}
            {!! Form::close() !!}
        </div>
    </div>
    
@endsection