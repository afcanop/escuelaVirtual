@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                    <h1 class="text-center">INFORMACIÓN DEL NIVEL {{$levelInformation->nivel}}</h1>
            </div>
            <div class="panel-body">
                    <span class="label label-info">DÍA</span>
                    <P>{{$levelInformation->dia}}</P>
                    <hr>
                    <span class="label label-info">Estado</span>
                    @if ($levelInformation->estado)
                        <P>termiando</P>
                        <hr>
                    @else
                        <P>actual</P>
                        <hr>
                    @endif
                    <span class="label label-info">DÍA</span>
                    <P>{{$levelInformation->dia}}</P>
                    <hr>
                    <span class="label label-info">PERIODO</span>
                    <P>{{$levelInformation->periodo}}</P>
                    <hr>
                    <span class="label label-info">FECHA DE INICIO DEL NIVEL</span>
                    <P>{{$levelInformation->fechaInicio}}</P>
                    <hr>
                    <span class="label label-info">FECHA DE FIN DEL NIVEL</span>
                    <P>{{$levelInformation->fechaFinal}}</P>
                    <hr>
            </div>
        </div>
    </div>
@endsection
{{ csrf_field() }}