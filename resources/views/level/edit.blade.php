@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h1>CREAR NIVEL</h1>
            </div>
            <div class="panel-body">
                {!! Form::model($levelInformation,  ['route'=>['EditarNivel.update', $levelInformation->id],  'method'=>'PUT' ] ) !!}
                @include('level.form')
                {{ csrf_field() }}
            {!! Form::close() !!} 
            </div>
        </div>
    </div>
@endsection