@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="thumbnail">
                    <div class="caption">
                        <h3 class="text-center text-capitalize">CREAR UN NUEVO NIVEL</h3>
                        <p class="text-center">
                            <a href="{{route('crearNivel')}}" class="btn btn-primary" role="button"><i class="glyphicon glyphicon-plus"></i></i> Button</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3 class="text-center text-capitalize">LISTA DE LOS NIVELES</h3>
                            <p class="text-center">
                                <a href="{{route('todos_los_niveles')}}" class="btn btn-primary" role="button"><i class="glyphicon glyphicon-th-list"></i></i> Button</a>
                            </p>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection