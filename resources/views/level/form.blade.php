
<div class="row form-group">
    <div class="col-sm-12 col-md-6">
        {!!  Form::label('nivel', 'NIVEL')  !!}
        {!! Form::select('nivel', 
            ['1' => '1', '2' => '2', '3' => '3', '4' => '4'],
        null, ['class' => 'form-control']
        ) !!}
    </div>
    <div class="col-sm-12 col-md-6">
        {!!  Form::label('dia', 'DIA')  !!}
        {!! Form::select('dia', 
            ['Lunes' => 'LUNES', 
            'Martes' => 'MARTES', 
            'Miercoles' => 'MIERCOLES', 
            'Jueves' => 'JUEVES',
            'Viernes' => 'VIERNES', 
            'Sabado_8' => 'SABADO 8', 
            'Sabado_10' => 'SABADO 10',
            'Sabado_1' => 'SABADO 1', 
            'Sabado_3' => 'SABADO 3',],
        null, ['class' => 'form-control']
        ) !!}
    </div>
</div>
<div class="row form-group">
    <div class="col-sm-12 col-md-6">
        {!!  Form::label('fechaInicio', 'FECHA INICIO')  !!}
        {!! Form::date('fechaInicio', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-sm-12 col-md-6">
        {!!  Form::label('fechaFinal', 'FECHA FINAL')  !!}
        {!! Form::date('fechaFinal', null, ['class' => 'form-control']) !!}
   </div>
</div>
<div class="row form-group">
    <div class="col-sm-12 col-md-6">
        {!!  Form::label('periodo', 'PERIODO')  !!}
        {!! Form::select('periodo', 
            ['1' => '1', '2' => '2'],
        null, ['class' => 'form-control']
        ) !!}
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="col-sm-12 col-md-6">
            <label>ESTADO</label>
            <div class="checkbox">
                <label>
                    {!! Form::checkbox('estado') !!} 
                    @isset($levelInformation->estado)
                        {{$levelInformation->estado ?'activo':'inactivo'}}
                    @else
                        estado
                    @endisset
                </label>
            </div>
        </div>
    </div>
</div>
<div class="row form-group">
    <div class="col-md-12">
        <button type="submit" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-floppy-saved"></span> GUARDAR</button>
    </div>
</div>