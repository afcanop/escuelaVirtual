@extends('layouts.app') @section('content')
    <div class="container">
        <div class="container">
            <h1>LISTA DE NIVELES</h1>
            <table class="table table-hover">
                    <thead>
                    <th class="text-center ">NIVEL</th>
                    <th class="text-center ">DÍA</th>
                    <th class="text-center ">FECHA INICO</th>
                    <th class="text-center ">FECHA FIN</th>
                    <th class="text-center ">PERIODO</th>
                    <th class="text-center ">ESTADO</th>

                    <th class="text-center " colspan='3'><span class="glyphicon glyphicon-cog"></span> OPCIONES</th>
                    </thead>
                    <tbody>
                    @foreach($allLevel as $level)
                        <tr>
                            <td class="text-center " > {{$level->nivel}} </td>
                            <td class="text-center " > {{$level->dia}} </td>
                            <td class="text-center " > {{$level->fechaInicio}} </td>
                            <td class="text-center " > {{$level->fechaFinal}} </td>
                            <td class="text-center " > {{$level->periodo}} </td>
                            <td class="text-center">                    @if($level->estado)<p>EN CURSO </p>@else<p>FINALIZADO</p>@endif                            </td>
                            <td>
                                <a href="{{ route('Informacion_Del_nivel',$level->id)}}" class="btn btn-info"> INFORMACIÓN</a>
                            </td>
                            <td>
                                <a href="{{ route('EditarNivel',$level->id)}}" class="btn btn-primary"> EDITAR</a>
                            </td>
                            <td>
                                <button type="button" class="btn btn-danger">ELIMINAR</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
            </table>

              {{$allLevel->render()}}
        </div>
    </div>
@endsection