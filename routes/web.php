<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'padres');

Auth::routes();

//padres
Route::get('padres', 'father\fatherController@index')->name('padres');
Route::get('todos_los_padres', 'father\fatherController@listFull')->name('todos_los_padres');
Route::get('Informacion_Del_Padre/{id}', 'father\fatherController@fatherInformation')->name('Informacion_Del_Padre');
Route::get('crearPadre', 'father\fatherController@create')->name('crearPadre');
Route::get('EditarPadre/{id}', 'father\fatherController@edit')->name('EditarPadre');
Route::POST('crearPadre.store', 'father\fatherController@store')->name('crearPadre.store');
Route::PUT('EditarPadre/{id}', 'father\fatherController@update')->name('EditarPadre.update');
Route::DELETE('EliminarPadre/{id}', 'father\fatherController@delete')->name('EliminarPadre');

//escuelas
Route::get('escuela', 'school\schoolController@index')->name('escuelas');
Route::get('todas_las_escuelas', 'school\schoolController@listFull')->name('todas_las_escuelas');
Route::get('escuela/{name}', 'school\schoolController@SpecificSchool')->name('escuela');
Route::get('crearEscuela', 'school\schoolController@create')->name('crearEscuela');
Route::get('EditarEscuela/{id}', 'school\schoolController@edit')->name('EditarEscuela');
Route::POST('crearEscuela.store', 'school\schoolController@store')->name('crearEscuela.store');
Route::PUT('EditarEscuela/{id}', 'school\schoolController@update')->name('EditarEscuela.update');
Route::DELETE('EliminarEscuela/{id}', 'school\schoolController@delete')->name('EliminarEscuela');

//nievel
Route::get('level', 'level\levelController@index')->name('level');
Route::get('todos_los_niveles', 'level\levelController@listFull')->name('todos_los_niveles');
Route::get('nivel/{id}','level\levelController@levelInformation')->name('Informacion_Del_nivel');
Route::get('crearNivel', 'level\levelController@create')->name('crearNivel');
Route::get('EditarNivel/{id}', 'level\levelController@edit')->name('EditarNivel');
Route::POST('crearNivel.store', 'level\levelController@store')->name('crearNivel.store');
Route::PUT('EditarNivel/{id}', 'level\levelController@update')->name('EditarNivel.update');
